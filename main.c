#include <util/delay.h>
#include <avr/io.h>

#define BIT_ON(PORT, BIT) PORT |= _BV(BIT);
#define BIT_OFF(PORT, BIT) PORT &= ~_BV(BIT);


int number2bcd[] = {
    ~0b00111111, //0
    ~0b00000110, //1 
    ~0b01011011, //2
    ~0b01001111, //3 
    ~0b01100110, //4
    ~0b01101101, //5
    ~0b01111101, //6
    ~0b00000111, //7
    ~0b01111111, //8
    ~0b01101111, //9
    ~0b01110111, //A
    ~0b01111100, //b
    ~0b00111001, //c
    ~0b01011110, //d
    ~0b01111011, //e 
    ~0b01001011, //f
    ~0b00111101, //g

};

int main() {
    DDRA |= _BV(DDA0); // set as output
    DDRA |= _BV(DDA1); // set as output
    DDRA |= _BV(DDA2); // set as output
    DDRA |= _BV(DDA3); // set as output
    DDRA |= _BV(DDA4); // set as output
    DDRA |= _BV(DDA5); // set as output
    DDRA |= _BV(DDA6); // set as output
    DDRA |= _BV(DDA7); // set as output

    DDRB |= _BV(DDB1); // set as output
    DDRB |= _BV(DDB2); // set as output

 
    while (1) {
        // BIT_OFF(PORTA, PORTA0);
        // BIT_OFF(PORTA, PORTA1);
        // BIT_ON(PORTA, PORTA2);
        // BIT_OFF(PORTA, PORTA3);
        // BIT_OFF(PORTA, PORTA4);
        // BIT_ON(PORTA, PORTA5);
        // BIT_OFF(PORTA, PORTA6);
        // BIT_OFF(PORTA, PORTA7);

        // PORTA = 255 - (1 << 7);
        // PORTA = ~ 0b00000001;
        PORTA = number2bcd[4];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);

        PORTA = number2bcd[5];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);


        PORTA = number2bcd[6];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);


        PORTA = number2bcd[7];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);



        PORTA = number2bcd[8];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);



        PORTA = number2bcd[9];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);



        PORTA = number2bcd[10];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);



        PORTA = number2bcd[11];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);




        PORTA = number2bcd[12];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);

        PORTA = number2bcd[13];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);




        PORTA = number2bcd[14];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);

        PORTA = number2bcd[15];
        BIT_ON(PORTB, PORTB1);
        BIT_ON(PORTB, PORTB2);
        _delay_ms(1000);
        BIT_OFF(PORTB, PORTB1);
        BIT_OFF(PORTB, PORTB2);        
        _delay_ms(1000);        
    }
 
    return 0;
}

